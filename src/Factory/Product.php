<?php

namespace App\Factory;

interface Product
{
	public function operation(): string;
}
