<?php

require '../../vendor/autoload.php';

use App\Factory\ConcreteCreator1;
use App\Factory\ConcreteCreator2;
use App\Factory\Creator;

function clientCode(Creator $creator): void
{
	echo "Client: I'm not aware of the creator's class, but it still works.\n" . $creator->someOperation();
}

echo "App: Launched with the ConcreteCreator1.\n";
clientCode(new ConcreteCreator1());
echo "\n\n";

echo "App: Launched with the ConcreteCreator2.\n";
clientCode(new ConcreteCreator2());
