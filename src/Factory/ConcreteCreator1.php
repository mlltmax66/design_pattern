<?php

namespace App\Factory;

class ConcreteCreator1 extends Creator
{
	public function factoryMethod(): Product
	{
		return new ConcreteProduct1();
	}
}
