<?php

namespace App\Factory;

class ConcreteProduct2 implements Product
{
	public function operation(): string
	{
		return "{Result of the ConcreteProduct2}";
	}
}
